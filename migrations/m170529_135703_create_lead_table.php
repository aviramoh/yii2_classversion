<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lead`.
 */
class m170529_135703_create_lead_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lead', [
            'id' => $this->primaryKey(),
			'name' => $this->String(),
			'email' => $this->String(),
			'phone' => $this->String(),
			'notes' => $this->text(),
			'status' => $this->Integer(),
			'owner' => $this->Integer(),
			'create_at' => $this->Integer(),
			'update_at' => $this->Integer(),
			'created_by' => $this->Integer(),
			'updated_by' => $this->Integer(),
        ],
		'ENGINE=InnoDB'
		);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('lead');
    }
}
