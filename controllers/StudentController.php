<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Student;


class StudentController extends Controller
{
	public function actionView($id)
    {
		$name = Student::getName($id); //we use :: beacuse its a static function
		return $this->render('view', ['name' => $name]);
    }  
}
