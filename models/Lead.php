<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "lead".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $notes
 * @property integer $status
 * @property integer $owner
 * @property integer $create_at
 * @property integer $update_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lead';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notes'], 'string'],
            [['status', 'owner', 'create_at', 'update_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	    /**
     * Defenition of relation to user table
     */ 	
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(), //מאכלס בערך הנכון את השדות מטה
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['create_at', 'update_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['update_at'],
				],
			],
		];
    }
	
	public function getUserOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner']);
    }
	
	/*public static function getOwners()
	{
		$allOwners = self::find()->all();
		$allOwnersArray = ArrayHelper::
					map($allStatuses, 'id', 'owner');
		return $allStatusesArray;						
	}
	
	public static function getOwnersWithAllOwners()
	{
		$allOwners = self::getOwners();
		$allOwners[null] = 'All Owners';
		$allStatuses = array_reverse ( $allOwners, true ); //all statuses will be first in the list
		return $allOwners;	
	}*/
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
    /**
     * Defenition of relation to status table
     */  
 	
 
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function beforeSave($insert)
	{
    if (!parent::beforeSave($insert)) {
        return false;
    }
         if ($this->isAttributeChanged('owner') && !\Yii::$app->user->can('changeOwner')) //the owner changed?
				$this->owner= $this->getOldAttribute('owner');
    return true;
	}
	
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'notes' => 'Notes',
            'status' => 'Status',
            'owner' => 'Owner',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
}
