<?php
//OwnLeadRule
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnLeadRule extends Rule
{
	public $name = 'ownLeadRule'; //must be here

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) { //is loged in
			return isset($params['lead']) ? $params['lead']->owner == $user : false;
		}
		return false;
	}
}